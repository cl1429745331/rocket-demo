package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName RocketApplication.java
 * @Description TODO
 * @createTime 2021年12月15日 20:46:00
 */
@SpringBootApplication
public class RocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(RocketApplication.class, args);
    }
}
